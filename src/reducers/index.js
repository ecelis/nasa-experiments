import { combineReducers } from 'redux';
import {
  GET_PICTURE_OF_THE_DAY
} from '../actions/PictureActions';

const initialState = {};

function nasa (state = initialState, action) {
  switch (action.type) {
    case GET_PICTURE_OF_THE_DAY:
      return Object.assign({},
        state);
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  nasa
});

export default rootReducer;
