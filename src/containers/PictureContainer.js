import { connect } from 'react-redux';
import PictureComponent from '../components/PictureComponent';

const mapStateToProps = state => {
  return {
    src: state.nasa.pod
  };
};

const PictureContainer = connect(mapStateToProps)(PictureComponent);

export default PictureContainer;
