import request from '../utils/nasaApi';

export const GET_PICTURE_OF_THE_DAY = 'GET_PICTURE_OF_THE_DAY';

export function getPictureOfTheDay (apod) {
  return {
    type: GET_PICTURE_OF_THE_DAY,
    pod: apod
  };
}

export function _getPictureOfTheDay () {
  return function (dispatch) {
    request({ api: 'apod' }).then(
      apod => {
        console.log(apod);
        dispatch(getPictureOfTheDay(apod));
      }
    );
  };
}
