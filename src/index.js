import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
//import registerServiceWorker from './registerServiceWorker';
import configureStore from './store';
import * as action from './actions/PictureActions';

const store = configureStore({ nasa: { title: 'NASA experiments' } });
store.dispatch(action._getPictureOfTheDay());

ReactDOM.render(
  <Provider store={store}><App /></Provider>,
  document.getElementById('root'));
//registerServiceWorker();
