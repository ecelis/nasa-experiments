import React from 'react';
import PropTypes from 'prop-types';

const PictureComponent = ({
  wrapperClass,
  imgClass,
  src,
  alt
}) => {
  return (
    <div className={wrapperClass}>
      <img className={imgClass}
        src={src}
        alt={alt} />
    </div>
  );
};

PictureComponent.propTypes = {
  wrapperClass: PropTypes.string,
  imgClass: PropTypes.string,
  src: PropTypes.string,
  alt: PropTypes.string
};
export default PictureComponent;
