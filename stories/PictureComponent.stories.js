import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import PictureComponent from '../src/components/PictureComponent';

storiesOf('PictureComponent', module)
  .add('APOD', () => (
    <PictureComponent
      wrapperClass="pod"
      imgClass="pod"
      src="https://api.nasa.gov/images/apod.jpg"
      alt="NASA PoD" />
  ));
